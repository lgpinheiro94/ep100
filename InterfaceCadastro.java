import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.SwingConstants;

@SuppressWarnings({ "unused", "serial" })
public class InterfaceCadastro extends JInternalFrame {
	private JTextField tfNome;
	private JTextField tfNomeUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceCadastro frame = new InterfaceCadastro(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public InterfaceCadastro(Pokedex pokedex) {
		setTitle("Cadastro de treinadores");
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setForeground(Color.WHITE);
		setClosable(true);
		setBounds(100, 100, 450, 323);
		
		JPanel panel = new JPanel();
		panel.setToolTipText("");
		panel.setBorder(new LineBorder(Color.RED, 5));
		panel.setBackground(new Color(0, 255, 0));
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(22, 12, 46, 14);
		panel.add(lblNome);
		
		JLabel lblIdade = new JLabel("Idade");
		lblIdade.setBounds(22, 70, 46, 14);
		panel.add(lblIdade);
		
		JLabel lblNomeDeUsuario = new JLabel("Nickname");
		lblNomeDeUsuario.setBounds(280, 12, 104, 14);
		panel.add(lblNomeDeUsuario);
		
		tfNome = new JTextField();
		tfNome.setBounds(22, 30, 184, 28);
		panel.add(tfNome);
		tfNome.setColumns(10);
		
		tfNomeUsuario = new JTextField();
		tfNomeUsuario.setBounds(280, 30, 122, 28);
		panel.add(tfNomeUsuario);
		tfNomeUsuario.setColumns(10);
		
		JComboBox cbSexo = new JComboBox();
		cbSexo.setToolTipText("");
		cbSexo.setModel(new DefaultComboBoxModel(new String[] {"Selecione", "Masculino", "Feminino"}));
		cbSexo.setBounds(22, 143, 111, 28);
		panel.add(cbSexo);
		
		JSpinner spIdade = new JSpinner();
		spIdade.setModel(new SpinnerNumberModel(10, 10, 130, 1));
		spIdade.setBounds(22, 86, 53, 28);
		panel.add(spIdade);
		
		JButton btnCadastrarTreinador = new JButton("Cadastrar");
		btnCadastrarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//pokedex.cadastrarTreinador(tfNome.getText(), (Integer) spIdade.getValue() , tfNomeUsuario.getText());
				JOptionPane.showMessageDialog(null, "Cadastrado com sucesso!");
				try {
					setClosed(true);
				} catch (PropertyVetoException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCadastrarTreinador.setBounds(280, 249, 142, 28);
		panel.add(btnCadastrarTreinador);
		
		JLabel lblNewLabel = new JLabel("Sexo");
		lblNewLabel.setBounds(22, 126, 66, 15);
		panel.add(lblNewLabel);
		
	}
}
