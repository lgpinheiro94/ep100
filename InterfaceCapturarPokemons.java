import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.SwingConstants;

@SuppressWarnings({ "unused", "serial" })
public class InterfaceCapturarPokemons extends JInternalFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceCapturarPokemons frame = new InterfaceCapturarPokemons(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public InterfaceCapturarPokemons(Pokedex pokedex) {
		setTitle("Cadastro de treinadores");
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setForeground(Color.WHITE);
		setClosable(true);
		setBounds(100, 100, 450, 323);
		
		JPanel panel = new JPanel();
		panel.setToolTipText("");
		panel.setBorder(new LineBorder(Color.RED, 5));
		panel.setBackground(new Color(0, 255, 0));
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JComboBox listadepokemons = new JComboBox();
		listadepokemons.setToolTipText("");
		listadepokemons.setModel(new DefaultComboBoxModel(new String[] {"Selecione"}));
		listadepokemons.setBounds(38, 55, 158, 28);
		panel.add(listadepokemons);
		
		JButton btnCadastrarTreinador = new JButton("Capturar");
		btnCadastrarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Capturado com sucesso!");
				try {
					setClosed(true);
				} catch (PropertyVetoException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCadastrarTreinador.setBounds(280, 249, 142, 28);
		panel.add(btnCadastrarTreinador);
		
		JLabel lblNewLabel = new JLabel("Pokemons");
		lblNewLabel.setBounds(38, 28, 66, 15);
		panel.add(lblNewLabel);
		
	}
}
