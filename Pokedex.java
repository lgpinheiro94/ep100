import java.util.ArrayList;

public class Pokedex {

	private ArrayList<Pokemon> pokemons;
	private ArrayList<Treinador> treinadores;
	private LeituraCSV LeituraCSV;
	
	public Pokedex() {
		this.setLeituraCSV(new LeituraCSV());
		this.pokemons = new ArrayList<Pokemon>();
		this.setTreinadores(new ArrayList<Treinador>());
	
	}
	
	public void pesquisaPokemon(String nome, Pokemon pokemon) {
		for(int i = 0; i < pokemons.size(); i++) {
		if(nome.equals(pokemons.get(i).getNome())) {			                
                pokemon.setNome(pokemons.get(i).getNome());
	            pokemon.setId(pokemons.get(i).getId());
	            pokemon.setImage(pokemons.get(i).getImage());
	            pokemon.setGeneration(pokemons.get(i).getGeneration());
	            pokemon.setTipo1(pokemons.get(i).getTipo1());
	            pokemon.setTipo2(pokemons.get(i).getTipo2());
	            pokemon.setHp(pokemons.get(i).getHp());
	            pokemon.setAtaque(pokemons.get(i).getAtaque());
	            pokemon.setDefesa(pokemons.get(i).getDefesa());
	            pokemon.setVelocidade(pokemons.get(i).getVelocidade());
	            pokemon.setLendario(pokemons.get(i).isLendario());
	            pokemon.setMove1(pokemons.get(i).getMove1());
	            pokemon.setMove2(pokemons.get(i).getMove2());
			}
		}

	}
	
	public void pesquisaTipoPokemon(String tipo) {
		
	}
	
	public void pesquisaPokemon(int id, Pokemon pokemon) {
		for(int i = 0; i < pokemons.size(); i++) {
			if(id == pokemons.get(i).getId()) {
				pokemon.setNome(pokemons.get(i).getNome());
	            pokemon.setId(pokemons.get(i).getId());
	            pokemon.setTipo1(pokemons.get(i).getTipo1());
	            pokemon.setTipo2(pokemons.get(i).getTipo2());
	            pokemon.setHp(pokemons.get(i).getHp());
	            pokemon.setAtaque(pokemons.get(i).getAtaque());
	            pokemon.setDefesa(pokemons.get(i).getDefesa());
                pokemon.setVelocidade(pokemons.get(i).getVelocidade());
	            pokemon.setLendario(pokemons.get(i).isLendario());
	            pokemon.setAltura((int)pokemons.get(i).getAltura());
	            pokemon.setPeso((int)pokemons.get(i).getPeso());
	            pokemon.setMove1(pokemons.get(i).getMove1());
	            pokemon.setMove2(pokemons.get(i).getMove2());
			}
		}

	}

	public boolean pesquisarTreinador(String nomeDeUsuario) {
		return true;
	}

	public void cadastrarTreinador(String nome, int idade, String sexo, String nomeDeUsuario) {
		
	}
	
	public void capturarPokemon(String nomeDeUsuario) {
		
	}
	
	public void obterPerfilTreinador(String nomeDeUsuario) {
		
	}

	public ArrayList<Treinador> getTreinadores() {
		return treinadores;
	}

	public void setTreinadores(ArrayList<Treinador> treinadores) {
		this.treinadores = treinadores;
	}

	public LeituraCSV getLeituraCSV() {
		return LeituraCSV;
	}

	public void setLeituraCSV(LeituraCSV leituraCSV) {
		LeituraCSV = leituraCSV;
	}
	
}
