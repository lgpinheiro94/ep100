import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class LeituraCSV {

	private static final String Arquivo = null;

	public static void Read(Pokemon[] Pokemons) {

		String conteudo = "data/csv_files/POKEMONS_DATA.csv";
		
		BufferedReader Conteudo = null;
		String Linha = "";
		String Divisor = ",";

		int i=0;
		
		try {
			
			Conteudo = new BufferedReader(new FileReader(Arquivo));

			Linha = Conteudo.readLine();
			
			while( (Linha = Conteudo.readLine()) != null ){
									
				String[] In = Linha.split(Divisor);
				
				Pokemons[i] = new Pokemon();
					
				Pokemons[i].setId(In[0]);
				Pokemons[i].setNome(In[1]);
				Pokemons[i].setTipo1(In[2]);
				Pokemons[i].setTipo2(In[3]);
				Pokemons[i].setHp(In[4]);
				Pokemons[i].setAtaque(In[5]);
				Pokemons[i].setDefesa(In[6]);
				Pokemons[i].setVelocidade(In[7]);
				Pokemons[i].setGeneration(In[8]);
				Pokemons[i].setLendario(In[9]);
				Pokemons[i].setAltura(In[10]);
				Pokemons[i].setPeso(In[11]);
				Pokemons[i].setMove1(In[12]);
				Pokemons[i].setMove2(In[13]);
				i++;
			}	
			
		} catch (FileNotFoundException e) {
			System.out.println("Arquivo não encontrado: "+e.getMessage());
		} catch (IOException e) {
			System.out.println("Io error: "+e.getMessage());
		}
	}
}
