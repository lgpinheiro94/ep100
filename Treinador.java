import java.util.ArrayList;

public class Treinador extends Usuario {
	
	private String nomeDeUsuario;
	private ArrayList<Pokemon>pokemon;
	
	public Treinador() {
		this.pokemon = new ArrayList<Pokemon>();
	}
	public ArrayList<Pokemon> getPokemons() {
		return pokemon;
	}
	public void setPokemons(Pokemon pokemon) {
		this.pokemon.add(pokemon);
	}
	public String getNomeDeUsuario() {
		return nomeDeUsuario;
	}
	public void setNomeDeUsuario(String nomeDeUsuario) {
		this.nomeDeUsuario = nomeDeUsuario;
	}	
}
